﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DownloadSpeedTest
{
    class Logger
    {
        public static void LogLine(string logline)
        {
            if (!Program.Options.Quiet)
            {
                Console.WriteLine("{0} - {1}", DateTime.Now, logline);
            }
        }
    }
}
