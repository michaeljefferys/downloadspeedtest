﻿using System.Text;
using CommandLine;
using CommandLine.Text;

namespace DownloadSpeedTest
{
    class Options
    {

        [Option('q', null, HelpText = "Do not print details during execution.")]
        public bool Quiet { get; set; }
        [Option("port",  DefaultValue = 9494, HelpText = "The port to host on")]
        public int Port { get; set; }

        [Option("delay", DefaultValue = 60, HelpText = "Initial delay between tests")]
        public int InitialDelay { get; set; }

        [Option("maxdelay", DefaultValue = 1200000, HelpText = "Max delay between tests")]
        public int MaxDelay { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}
