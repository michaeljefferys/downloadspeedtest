﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DownloadSpeedTest
{
    class SpeedTestServer
    {
        public SpeedTestServer()
        {
            UseSsl = false;
            NextTest = DateTime.MinValue;
        }

        public string Name { get; set; }
        public string Host { get; set; }
        public string BaseURL { get; set; }
        public SpeedTestFile.FileType LastFileType { get; set; }
        public Boolean UseSsl { get; set; }
        public DateTime NextTest { get; set; }
        public DateTime LastTest { get; set; }
        public DownloadTester LastTestObject { get; set; }

        public string TestUrlBase
        {
            get
            {
                if (UseSsl)
                {
                    return !String.IsNullOrEmpty(BaseURL) ? $"https://{Host}/{BaseURL}" : $"https://{Host}";
                }
                return !String.IsNullOrEmpty(BaseURL) ? $"http://{Host}/{BaseURL}" : $"http://{Host}";

            }
        }

        
    }
}
