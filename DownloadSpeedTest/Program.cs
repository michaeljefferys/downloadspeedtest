﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.Hosting;
using Microsoft.Owin.StaticFiles;
using Owin;
using CommandLine;
using CommandLine.Text;

namespace DownloadSpeedTest
{
    class Program
    {
        public static bool Quiet;
        public static string HtmlPath = String.Empty;
        public static Options Options;
        public static List<SpeedTestServer> SpeedTestServers = new List<SpeedTestServer>();

        static void Main(string[] args)
        {
            Options = new Options();
            if (Parser.Default.ParseArgumentsStrict(args, Options))
            {
                if (!Options.Quiet)
                {
                    Console.Write(Options.GetUsage());
                }
                Logger.LogLine(String.Format("Setting self host port to {0}", Options.Port));
                var port = Options.Port;

                // Set up html path
                var excecutingpath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                var seperator = Path.DirectorySeparatorChar;
                HtmlPath = String.Format("{0}{1}HTML{1}", excecutingpath, seperator);

                // Set up self host
                SetupSelfHost(port);

                // Set config constants
                // Url to file to download
                // Todo move to speedTestServer class?
                //string[] speedTestServer = { "tandyuk.com/bb", "jupiter.undisclosedpower.net:8069", "venus.undisclosedpower.net:8069" };
                //string[] speedTestServer = { "jupiter.undisclosedpower.net:8069" };



                SpeedTestServer tandyuk = new SpeedTestServer { Host = "tandyuk.com", BaseURL = "bb", Name = "TandyUK", UseSsl = false };
                SpeedTestServers.Add(tandyuk);
                //SpeedTestServer jupiter = new SpeedTestServer { Host = "jupiter.undisclosedpower.net:8069", BaseURL = "", Name = "Jupiter" };
                //SpeedTestServers.Add(jupiter);
                //SpeedTestServer venus = new SpeedTestServer { Host = "venus.undisclosedpower.net:8069", BaseURL = "", Name = "Venus" };
                //SpeedTestServers.Add(venus);
                
                SpeedTestServer saturn = new SpeedTestServer { Host = "saturn.undisclosedpower.net:8069", BaseURL = "", Name = "Saturn" };
                SpeedTestServers.Add(saturn);

                var speedTestFilesList = new List<SpeedTestFile>();
                var speedTest10 = new SpeedTestFile("10Mio.dat", SpeedTestFile.FileType.Small);
                var speedTest20 = new SpeedTestFile("20mio.dat", SpeedTestFile.FileType.Medium);
                var speedTest50 = new SpeedTestFile("50mio.dat", SpeedTestFile.FileType.Large);
                speedTestFilesList.Add(speedTest10);
                speedTestFilesList.Add(speedTest20);
                speedTestFilesList.Add(speedTest50);

                // How often to download
                int delay = Options.InitialDelay;
                // Set up internal constants
                // Todo move delays to filetype class?
                int smallDelay = delay * 1000;
                int midDelay = 100000;
                double largeDelay = 300000;

                Logger.LogLine(String.Format("Tests will initially repeat every {0} seconds", delay));

                while (true)
                {
                    var currentFileType = SpeedTestFile.FileType.Small;
                    foreach (var server in SpeedTestServers)
                    {
                        if (server.NextTest < DateTime.Now || server.NextTest == DateTime.MinValue)
                        {
                            Logger.LogLine(String.Format("Test commencing using files from: {0}", server.Name));
                            Logger.LogLine("ctrl+c to break");
                            server.LastTest = DateTime.Now;
                            try
                            {
                                foreach (var speedTestFile in speedTestFilesList)
                                {
                                    string testUrl = $"{server.TestUrlBase}/{speedTestFile.FileName}";
                                    DownloadTester tester = new DownloadTester(testUrl);
                                    var tsw = tester.DownloadStopWatch();
                                    Logger.LogLine(
                                        String.Format(
                                            "Download of {1} bytes complete in {0} at an average of {2} bytes per second",
                                            tsw.Elapsed, tester.TotalBytes, tester.BytesPerSecond));


                                    server.LastTestObject = tester;
                                    if (tsw.Elapsed.TotalSeconds > 3 &&
                                        speedTestFile.FileTypeEnum == SpeedTestFile.FileType.Small)
                                    {
                                        server.LastFileType = SpeedTestFile.FileType.Small;
                                        server.NextTest = DateTime.Now.AddMilliseconds(smallDelay);
                                        currentFileType = SpeedTestFile.FileType.Small;
                                        FileLogger.WriteToCSVFiles(tsw.Elapsed, tester.BytesPerSecond,
                                            tester.TotalBytes,
                                            server.Name);
                                        break;
                                    }

                                    if (tsw.Elapsed.TotalSeconds > 5 &&
                                        speedTestFile.FileTypeEnum == SpeedTestFile.FileType.Medium)
                                    {
                                        server.LastFileType = SpeedTestFile.FileType.Medium;
                                        currentFileType = SpeedTestFile.FileType.Medium;
                                        server.NextTest = DateTime.Now.AddMilliseconds(midDelay);
                                        FileLogger.WriteToCSVFiles(tsw.Elapsed, tester.BytesPerSecond,
                                            tester.TotalBytes,
                                            server.Name);
                                        break;
                                    }

                                    if (speedTestFile.FileTypeEnum == SpeedTestFile.FileType.Large)
                                    {
                                        currentFileType = SpeedTestFile.FileType.Large;
                                        server.LastFileType = SpeedTestFile.FileType.Large;
                                        largeDelay = GetLargeDelay(largeDelay);
                                        server.NextTest = DateTime.Now.AddMilliseconds(largeDelay);
                                        FileLogger.WriteToCSVFiles(tsw.Elapsed, tester.BytesPerSecond,
                                            tester.TotalBytes,
                                            server.Name);
                                        break;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LogLine(ex.Message);
                            }
                        }
                    }
                    var lastFileType = currentFileType;
                    //easy mode check when all the servers get the same file we can just let it rerun willy nilly
                    if (SpeedTestServers.Count(x => x.LastFileType == SpeedTestFile.FileType.Small) ==
                        SpeedTestServers.Count)
                    {
                        Logger.LogLine($"Small file was downloaded for everyone, sleeping for {smallDelay}");
                        largeDelay = 300000;
                        Thread.Sleep(smallDelay);
                        ResetServers();
                    }
                    else if (SpeedTestServers.Count(x => x.LastFileType == SpeedTestFile.FileType.Medium) ==
                             SpeedTestServers.Count)
                    {
                        Logger.LogLine($"Mid file was downloaded for everyone, sleeping for {midDelay}");
                        largeDelay = 300000;
                        Thread.Sleep(midDelay);
                        ResetServers();
                    }
                    else if (SpeedTestServers.Count(x => x.LastFileType == SpeedTestFile.FileType.Large) ==
                             SpeedTestServers.Count)
                    {
                        // Don't ever let the test wait more than about 20 minutes
                        if (lastFileType == currentFileType & largeDelay < Options.MaxDelay)
                        {
                            largeDelay = largeDelay * 1.2;
                        }
                        else if (lastFileType == currentFileType)
                        {
                            largeDelay = Options.MaxDelay;
                        }
                        else
                        {
                            largeDelay = 600000;
                        }
                        Logger.LogLine(String.Format("Large file was downloaded for everyone, sleeping for {0}", largeDelay));
                        Thread.Sleep((int)largeDelay);
                        ResetServers();
                    }
                    else
                    {
                        foreach (var server in SpeedTestServers)
                        {
                            Logger.LogLine(
                                $"{server.Name} next scheduled for ~{server.NextTest.ToShortTimeString()}, last tested {server.LastTest.ToShortTimeString()} and download of {server.LastTestObject.TotalBytes} bytes completed at {server.LastTestObject.BytesPerSecond} bytes per second");
                        }
                        Thread.Sleep(smallDelay);
                    }
                }

            }
        }

        private static double GetLargeDelay(double largeDelay)
        {
            if (largeDelay < Options.MaxDelay)
            {
                largeDelay = largeDelay * 1.2;
            }
            else
            {
                largeDelay = Options.MaxDelay;
            }
            return largeDelay;
        }

        private static void ResetServers()
        {
            foreach (var server in SpeedTestServers)
            {
                server.NextTest = DateTime.MinValue; ;
            }
        }

        private static void SetupSelfHost(int port)
        {
            string url = String.Format("http://*:{0}", port);
            var fileSystem = new PhysicalFileSystem(HtmlPath);
            var options = new FileServerOptions
            {
                EnableDirectoryBrowsing = false,
                FileSystem = fileSystem
            };
            try
            {
                WebApp.Start(url, builder => builder.UseFileServer(options));
            }
            catch (Exception)
            {
                // Bind to localhost if we cant bind to all ports
                url = String.Format("http://localhost:{0}", port);
                Console.ForegroundColor = ConsoleColor.Red;
                WebApp.Start(url, builder => builder.UseFileServer(options));
                Logger.LogLine("Unable to bind to all IPs");
                Logger.LogLine("Please run as administrator to bind to all IPs");
                Console.ResetColor();
                Logger.LogLine(String.Format("Selfhost listening at: {0}", url));
                Logger.LogLine("Tests will start in 10 seconds");
                Thread.Sleep(10000);
            }
            Logger.LogLine(String.Format("Selfhost listening at: {0}", url));
        }

    }
}

