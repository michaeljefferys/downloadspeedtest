﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;

namespace DownloadSpeedTest
{
    class DownloadTester
    {
        public int TotalBytes;
        public int BytesRecieved;
        public int ProgressPercentage;

        public DateTime Started;
        public DateTime Ended;

        private readonly Uri _downloadUri;
        private readonly string _fileName;
        private readonly Stopwatch _sw = new Stopwatch();

        public DownloadTester(string url)
        {
            _downloadUri = new Uri(url);
            _fileName = Path.GetFileName(_downloadUri.LocalPath);
        }

        public int BytesPerSecond
        {
            get { return (int) (TotalBytes / _sw.Elapsed.TotalSeconds); }
        }

        public Stopwatch DownloadStopWatch()
        {
            Started = DateTime.Now;

            // Set up Webclient And Handlers
            WebClient wc = new WebClient();
            wc.DownloadProgressChanged += WcOnDownloadProgressChanged;
            wc.DownloadFileCompleted += WcOnDownloadFileCompleted;

            // Start the stopwatch
            _sw.Start();
            //wc.DownloadFileAsync(_downloadUri, _fileName);

            wc.DownloadDataAsync(_downloadUri);;
            Logger.LogLine(String.Format("Starting test with file {0}", _downloadUri));


            // Hangtimer, if download thread is running for more than 10 minutes break out.
            var hangTimer = new Stopwatch();
            hangTimer.Start();

            while (wc.IsBusy)
            {
                // Monitor the status every 100;
                Logger.LogLine(String.Format("Downloaded {0}/{1} {2}%", BytesRecieved, TotalBytes, ProgressPercentage));
                Thread.Sleep(100);

                // Timer has run for more than 10 minutes, stop, reset, and return
                if (hangTimer.Elapsed.Minutes >= 10)
                {
                    _sw.Stop();
                    _sw.Reset();
                    return _sw;
                }
            }

            // Reset the hangtimer too (shouldn't be needed as each download is a new instance, but just in case)
            hangTimer.Reset();
            // Download is done, stop and return
            _sw.Stop();
            Ended = DateTime.Now;
            return _sw;
        }

        private void WcOnDownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Logger.LogLine("File done");
        }

        private void WcOnDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            TotalBytes = (int)e.TotalBytesToReceive;
            BytesRecieved = (int)e.BytesReceived;
            ProgressPercentage = e.ProgressPercentage;
        }
    }
}
