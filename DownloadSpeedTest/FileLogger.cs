﻿using System;
using System.IO;

namespace DownloadSpeedTest
{
    class FileLogger
    {
        public static void WriteToCSVFiles(TimeSpan elapsed, int bytesPerSecond, int totalBytesRecieved, string server)
        {
            server = server.Replace(":", "");
            server = server.Replace("/", "");
            WriteLongLog(elapsed, bytesPerSecond, totalBytesRecieved, server);
            WriteShortLog(bytesPerSecond, server);
        }

        private static void WriteShortLog(int bytesPerSecond, string server)
        {
            server = String.Format("{0}{1}", Program.HtmlPath, server);
            string filename = String.Format("{0}.shortlog.csv", server);
            string headerstring = "Date, BPS";
            WriteHeader(filename, headerstring);
            // date format for dygraphs
            // 2009/07/12 12:34:56
            string now = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            string shortlog = String.Format("{0},{1}", now, bytesPerSecond);
            StreamWriter shortStreamWriter = File.AppendText(filename);
            shortStreamWriter.WriteLine(shortlog);
            shortStreamWriter.Close();
        }

        private static void WriteLongLog(TimeSpan elapsed, int bytesPerSecond, int totalBytesRecieved, string server)
        {
            server = String.Format("{0}{1}", Program.HtmlPath, server);
            string filename = String.Format("{0}.fullog.csv", server);
            string header = "Date,Duration,TotalBytes,BytesPerSecond";
            WriteHeader(filename, header);
            string line = String.Format("{0},{1},{2},{3}", DateTime.Now, elapsed, totalBytesRecieved, bytesPerSecond);
            StreamWriter streamWriter = File.AppendText(filename);
            streamWriter.WriteLine(line);
            streamWriter.Close();
        }

        private static void WriteHeader(string filename, string header)
        {
            if (!File.Exists(filename))
            {
                StreamWriter streamWriter = File.AppendText(filename);
                if (!String.IsNullOrEmpty(header))
                {
                    streamWriter.WriteLine(header);
                }
                streamWriter.Close();
            }
        }
    }
}
