﻿namespace DownloadSpeedTest
{
    class SpeedTestFile
    {
        public SpeedTestFile(string fileName, FileType fileType)
        {
            FileName = fileName;
            FileTypeEnum = fileType;
        }

        public string FileName { get; set; }

        public FileType FileTypeEnum { get; set; }

        public enum FileType
        {
            Small,
            Medium,
            Large
        }
    }
}
